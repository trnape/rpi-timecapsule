#Time Capsule for Raspberry Pi
FROM resin/rpi-raspbian:jessie

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends supervisor netatalk

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ADD run.sh /run.sh

EXPOSE 548

CMD ["/bin/sh", "/run.sh"]
